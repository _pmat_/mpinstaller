# mpinstaller

This utility helps installing/upgrading MXLinux >= 18 on a LVM2 partition scheme.

Pure bashscripting for ease of use.

The author is not responsible for any loss or damages arising from the use of this script. USE AT YOUR OWN RISK.

## USAGE
Before using this script you have to setup some variable inside the header of itself.

To keep it simple it only supports a LVM2 partition scheme with 1 volume group and 2 logical volume: ROOT and HOME.

This script allows to add a single user.


HEADER TO SETUP
```
LVROOT=/dev/vg00/lvroot
LVHOME=/dev/vg00/lvhome
DESTROOT=/srv/root
DESTHOME=/srv/home
MYUSER=whatever
```

You can put this small shellscript inside the usb key you'll write the iso (e.g. inside the partition 2 of the usb key prepared with dd or stuff like that) and then mount it while the live is running.