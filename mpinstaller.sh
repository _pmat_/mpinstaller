#!/bin/sh

# (C) 2018-2019 by Matteo Pasotti <matteo.pasotti@gmail.com>
# License: GPLv3
# installer/updater for MXLinux running on LVM2 partition scheme
# version 1.1


# basic setup
LVROOT=/dev/vg00/lvroot
LVHOME=/dev/vg00/lvhome
LVSWAP=/dev/vg00/lvswap
DESTROOT=/srv/root
DESTHOME=/srv/home
MYUSER=youruser



# TO RUN WITH ROOT PRIVILEGES
if [ "$(whoami)" != 'root' ];
then
	echo "$0 MUST BE RUN AS ROOT"
	exit 1
fi

# fix DNS resolver
# echo "[*] Add DNS address"
# echo "8.8.8.8" >> /etc/resolv.conf

# BE SURE TO HAVE INTERNET CONNECTION
echo "[*] Checking internet connection"
RES=`LC_ALL=C ping -c1 www.google.com`
if [ "${RES}" = 'ping: www.google.com: Name or service not known' ];
then
	echo "[*] No internet connection"
	exit 2
fi

# install prerequisites
echo "[*] Install pre-requisites"
apt update
apt install -y lvm2 rsync

echo "[*] Setup logical volumes"
vgscan --mknodes
vgchange -ay
lvscan
lvdisplay | grep -i "LV PATH"

if [ ! -d "${DESTROOT}" ];
then
	mkdir $DESTROOT
	mkdir $DESTHOME
fi

echo "[*] Mounting LVHOME (readonly to keep it safe)"
mount $LVROOT $DESTROOT
mount -o ro $LVHOME $DESTHOME

HOME_FSTYPE=`mount | grep home | awk -F'type' '{print $2}' | awk '{print $1}'`
read -p "Do you want to proceed?" WHATEVER

ROOT_FSTYPE="ext4"
echo "[*] Formatting LVROOT with FS=${ROOT_FSTYPE}"
umount $LVROOT
mkfs.$ROOT_FSTYPE $LVROOT

echo "[*] Mounting LVROOT (readonly to keep it safe)"
mount $LVROOT $DESTROOT
read -p "Do you want to proceed?" WHATEVER

# copying data
echo "[*] Copying data..."
rsync -avP --exclude=/srv --exclude=/home --exclude=/mnt --exclude=/live --exclude=/dev --exclude=/proc --exclude=/sys --exclude=/tmp /. $DESTROOT/

# mount virtual fs on $DESTROOT
# proc
# sys
echo "[*] Mounting virtual fs"
mkdir $DESTROOT/proc
mkdir $DESTROOT/sys
mkdir $DESTROOT/dev
mkdir $DESTROOT/tmp
mkdir $DESTROOT/home

chmod 1777 $DESTROOT/tmp
find $DESTROOT/tmp -mindepth 1 -name '.*-unix' -exec chmod 1777 {} + -prune -o -exec chmod go-rwx {} +

mount --bind /proc $DESTROOT/proc
mount --bind /sys $DESTROOT/sys
mount --bind /dev $DESTROOT/dev
# chroot $DESTROOT
echo "[*] Installing grub"
chroot $DESTROOT grub-install /dev/sda
echo "[*] Updating grub"
chroot $DESTROOT update-grub
echo "[*] Adding user"
useradd -d /home/$MYUSER -M -s /bin/bash -G sudo $MYUSER

# remount $DESTHOME as it was mounted readonly
umount $DESTHOME
mount $LVHOME $DESTHOME

chown -R $MYUSER.$MYUSER /home/$MYUSER

echo "[*] Syncing fstab"
echo "${LVROOT}   /   ${ROOT_FSTYPE}   defaults,noatime   0   1" >> $DESTROOT/etc/fstab
echo "${LVHOME}   /home   ${HOME_FSTYPE}   defaults   0   2" >> $DESTROOT/etc/fstab
echo "${LVSWAP}   none    swap   defaults   0   0" >> $DESTROOT/etc/fstab

umount $DESTROOT/proc
umount $DESTROOT/sys
umount $DESTROOT/dev

echo "[*] Upgrade finished"
exit 0
